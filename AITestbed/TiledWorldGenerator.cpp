#include "TiledWorldGenerator.h"
#include "Tile.h"
#include "imgui_internal.h"
#include <iostream>
#include <algorithm>
#include <list>
#include "AABB.h"

using namespace std;

const float WindowBuffer = 5.0f;
const float CellBorder = 1.0f;

class QuadTree
{
	int QT_NODE_CAPACITY = 4;
	AABBf boundary;

	vector<Tile*> pointers;

	QuadTree* TopLeft;
	QuadTree* TopRight;
	QuadTree* BotLeft;
	QuadTree* BotRight;



	//vector<Tile*>& Search();

public:
	QuadTree(Vector2f BoxMin, Vector2f BoxMax)
	{
		boundary.boxMin = BoxMin;
		boundary.boxMax = BoxMax;
		TopLeft = nullptr;
		TopRight = nullptr;
		BotLeft = nullptr;
		BotRight = nullptr;
	}

	void insert(Tile* tileToAdd)
	{
		// Check if the tile being added generates a potential field(the tile's field range will be greater than 0). 
		if (tileToAdd->FieldRange <= 0)
			return;

		// If the tile does generate a field then create a bounding box. The bounding box will be centred on the tile's location AND its width and height will be the tile's field range * 2 (the field is circular but a box works)
		AABBf NewBoundary;
		NewBoundary.boxMax = Vector2f(tileToAdd->Location.X + tileToAdd->FieldRange, tileToAdd->Location.Y + tileToAdd->FieldRange);
		NewBoundary.boxMin = Vector2f(tileToAdd->Location.X - tileToAdd->FieldRange, tileToAdd->Location.Y - tileToAdd->FieldRange);

		//When the function is run, check if the node has been split(if any of top left, top right etc are not nullptr)(Bool)
		if (TopLeft != nullptr)
		{
			// Find the appropriate child to add the tile to by checking if the tile's bounding box overlaps the bounding box for the top left, top right, bottom left or bottom right nodes
			// AABB intersect
			// For each node that the tile overlaps run the add tile function for that node and provide it with the pointer to the tile

			if (TopLeft->boundary.Intersects(NewBoundary))
			{
				TopLeft->insert(tileToAdd);
			}
			if (TopRight->boundary.Intersects(NewBoundary))
			{
				TopRight->insert(tileToAdd);
			}
			if (BotLeft->boundary.Intersects(NewBoundary))
			{
				BotLeft->insert(tileToAdd);
			}
			if (BotRight->boundary.Intersects(NewBoundary))
			{
				BotRight->insert(tileToAdd);
			}


			// node is not split
			else
			{
				pointers.push_back(tileToAdd);

				//Check if the node is over some threshold number of tiles(eg. 20) AND if the node is larger than a set size(eg.node is larger than 15x15)
				if (pointers.size() > 20 && boundary.Height() > 15 && boundary.Width() > 15)
				{
					TopLeft =	new QuadTree(Vector2f(boundary.boxMin.X, (boundary.Height()/2 + boundary.boxMin.Y)), 
											 Vector2f((boundary.Width()/2 + boundary.boxMin.X), boundary.boxMax.Y) );

					TopRight =	new QuadTree(Vector2f(boundary.Width() / 2, boundary.Height() / 2), boundary.boxMax);
					BotLeft =	new QuadTree(boundary.boxMin, boundary.boxMin + Vector2f(boundary.Width() / 2, boundary.Height() / 2));
					BotRight =	new QuadTree(boundary.boxMin + Vector2f(boundary.Width()/2,0), Vector2f(boundary.boxMax.X, boundary.boxMin.Y + boundary.Height()/2));

					// Loop over all of the tiles in the std::vector of tiles and add them to the top left, top right etc nodes as appropriate (use the same logic as when the node was split)

					for (Tile* tile : pointers)
					{
						if (TopLeft->boundary.Intersects(NewBoundary))
						{
							TopLeft->insert(tile);
						}
						if (TopRight->boundary.Intersects(NewBoundary))
						{
							TopRight->insert(tile);
						}
						if (BotLeft->boundary.Intersects(NewBoundary))
						{
							BotLeft->insert(tile);
						}
						if (BotRight->boundary.Intersects(NewBoundary))
						{
							BotRight->insert(tile);
						}


					}
					//Clear the std::vector of tile pointers on this node as they're now all stored on the children
					pointers.clear();

				}
			}

		}
	}
	vector<Tile*>& Search(Vector2f Loc)
	{
		//When it is run it checks if the std::vector contains any items(ie.the size is greater than 0)
		if (pointers.size() > 0)
			return pointers;


		//If the std::vector is empty then it checks which of the children(top left, top right etc) contains the tile's location 
		//and runs the retrieval function on that child and returns the result

		if (TopLeft->boundary.Contains(Loc))
			return TopLeft->Search(Loc);
		
		if (TopRight->boundary.Contains(Loc))
			return TopLeft->Search(Loc);

		if (BotRight->boundary.Contains(Loc))
			return TopLeft->Search(Loc);

		if (BotLeft->boundary.Contains(Loc))
			return TopLeft->Search(Loc);
		
	}
};

void TiledWorldGenerator::Generate() 
{
	// perform the world generation
	NormaliseProbabilities();
	ClearWorld();
	GenerateWorld();

	
}


void TiledWorldGenerator::CalculateField()
{
	largestFieldStrength = 0;
	QuadTree Quad(Vector2f::Zero, Vector2f(Width,Length));
	for (Tile* AllTiles : world)
	{
		Quad.insert(AllTiles);
	}
	// iterate over the tiles and calculate their field
	for (Tile* currentTilePtr : world)
	{
		// reset the field
		currentTilePtr->LocalFieldValue = Vector2f::Zero;

		// is this an obstacle? if so do nothing
		if (currentTilePtr->Type == ettObstructed)
			continue;
		vector<Tile*> relevantTiles = Quad.Search(currentTilePtr->Location);

		// iterate over every other tile and add their contribution to the field
		for(Tile* otherTilePtr : relevantTiles)
		{
			// skip this tile
			if (otherTilePtr == currentTilePtr)
				continue;

			currentTilePtr->LocalFieldValue += otherTilePtr->CalculateFieldTo(currentTilePtr);
		}

		// track the largest field strength
		float fieldStrength = currentTilePtr->LocalFieldValue.Magnitude();
		if (fieldStrength > largestFieldStrength)
			largestFieldStrength = fieldStrength;
		
	}
}




void TiledWorldGenerator::DrawWorld()
{
	// early out if there is no world
	if (world.size() == 0)
		return;

	// grab the window
    ImGuiWindow* window = ImGui::GetCurrentWindowRead();
	
	// determine the cell size
	ImVec2 windowSize = ImGui::GetWindowSize();
	int cellSize = (int) std::min((windowSize.x - (WindowBuffer * 2)) / Length, (windowSize.y - window->TitleBarHeight() - (WindowBuffer * 2)) / Width);

	// get the draw list to update
	ImDrawList* drawList = ImGui::GetWindowDrawList();

	// get the window pos
	ImVec2 startPoint = ImGui::GetWindowPos();
	startPoint.x += WindowBuffer;
	startPoint.y += window->TitleBarHeight() + WindowBuffer;

	// draw the tiles
	for(Tile* tilePtr : world)
	{
		// calculate the tile location
		ImVec2 location = ImVec2((tilePtr->Location.X * cellSize) + startPoint.x, (tilePtr->Location.Y * cellSize) + startPoint.y);
		ImColor workingColour = tilePtr->Colour;

		// add the cell bounds
		//drawList->AddRect(location, ImVec2(location.x + cellSize, location.y + cellSize), 0xFFFFFFFF);

		// normalise the field
		if (ShowField && largestFieldStrength > 0)
		{
			Vector2f localField = tilePtr->LocalFieldValue.Normalised();// / largestFieldStrength;
			workingColour = ImColor(0.5f + (localField.X / 2.0f), 
									0.5f + (localField.Y / 2.0f), 
									0.0f);
		}

		// draw the cell
		drawList->AddRectFilled(ImVec2(location.x + CellBorder, location.y + CellBorder), 
						        ImVec2(location.x + cellSize - CellBorder*2, location.y + cellSize - CellBorder*2),
								workingColour);
	}

	////////////////////////////////////////////////////////////////////////////////
	// TODO: Add any debug drawing here. You can use drawList to draw lines etc
	////////////////////////////////////////////////////////////////////////////////
}

void TiledWorldGenerator::NormaliseProbabilities()
{
	// sum all of the tile frequencies
	int frequencySum = 0;
	for(AvailableTile* tilePtr : TilePalette)
	{
		frequencySum += tilePtr->Frequency;
	}

	// set the overall probability thresholds
	float currentThreshold = 0;
	for (AvailableTile* tilePtr : TilePalette)
	{
		currentThreshold += (float)tilePtr->Frequency / (float)frequencySum;
		tilePtr->Threshold = currentThreshold;
	}
}

void TiledWorldGenerator::ClearWorld()
{
	// cleanup the world
	for (Tile* tilePtr : world)
	{
		delete tilePtr;
	}
	world.clear();
}

void TiledWorldGenerator::GenerateWorld()
{
	// tile palette is empty so early out
	if (TilePalette.size() == 0)
		return;

	// reserve space for the world
	world.reserve(Length * Width);

	// generate the world
	for (int lengthIndex = 0; lengthIndex < Length; ++lengthIndex)
	{
		for (int widthIndex = 0; widthIndex < Width; ++widthIndex)
		{
			// roll a random number from 0 to 1
			float roll = (float)(rand() % 101) / 100.0f;

			// select matching reference tile (default is pure random)
			AvailableTile* referenceTilePtr = nullptr;
			for(AvailableTile* tilePtr : TilePalette)
			{
				if (roll <= tilePtr->Threshold)
				{
					referenceTilePtr = tilePtr;
					break;
				}
			}
			if (!referenceTilePtr)
				referenceTilePtr = TilePalette[rand() % TilePalette.size()];
			 
			// instantiate the new tile
			world.push_back(new Tile(referenceTilePtr->Type, referenceTilePtr->Colour, 
									 Vector2f((float)lengthIndex, (float)widthIndex), 
									 referenceTilePtr->FieldStrength, referenceTilePtr->FieldRange));
		}
	}
}





